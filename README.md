delExpiredAttributes.py is a python script designed to search for specific tags in MISP events and remove them after a set expiration date.

It is dependent on the pyMISP module.
You will need to add a keys.py file with your MISP key and MISP URL.  You can get more information from the pyMISP project: https://github.com/MISP/PyMISP

The latest code is modifyAttributes.py, it has added functionality.  It doesn't delete the the Attributes anymore, just moves them and deletes the tags.  It also now sets the ids tag.
