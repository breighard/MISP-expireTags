#!/usr/local/dev/python_enviroments/pymispenv/bin/python3
# -*- coding: utf-8 -*-
'''
##############################
# Brad Reighard
# 05/30/18
# This script is desinged to run on MISP
# It will remove attributes that are tagged and past expiration
    #Versions:
    #0.1 - Default functionality
    #0.2 - Now able to log to syslog and screen, also added test functionality
    #0.3 - Add Expired tag instead of deleting an attribute
    #0.4 - Added verbose logging to screen
    #0.5 - Changes to_ids from true to false
    #0.6 - Keeps events published after changing to_ids property 

#usr/local/dev - Run from dev for test
#usr/loca/bin  - Run in prod.
# cron: 
#0 1 * * * root cd /usr/local/dev/ && python_enviroments/pymispenv/bin/python3 delExpiredAttributes.py -t
##############################
'''

from pymisp import PyMISP
from pymisp import MISPObject
from pymisp import MISPAttribute
from keys import misp_url, misp_key, misp_verifycert
import argparse
import os
import sys
import syslog
import warnings
import calendar
import time
import datetime
import json

##############
# VARS
##############
VERSION = "0.6"
#Time in seconds for EPOC
#TIME30DAYS = 1  #Edit for testing!
TIME30DAYS = 2592000  
TIME60DAYS = 5184000
TIME90DAYS = 7776000
TIME180DAYS = 15552000
TIME365DAYS = 31536000
#These tag names must match the tag names in MISP exactly!
TAG30DAY = 'Expire_30_Days'
TAG60DAY = 'Expire_60_Days'
TAG90DAY = 'Expire_90_Days'
TAG180DAY = 'Expire_180_Days'
TAG365DAY = 'Expire_360_Days'
TAG_EXPIRED = 'Expired'
#These tags are used to block content
REMOVE_BLOCKING_TAGS = ['RPZLocalhost', 'RPZURLFILTER', 'RPZWALLEDGARDEN' ]
#Current Time in EPOC
TIMECURRENT = calendar.timegm(time.gmtime())


##############
# Functions
##############


def get_args():
    """Get arguments from system

    """
    parser = argparse.ArgumentParser(description='Remove attributes that are tagged to expire.')
    parser.add_argument("-v", "--verbose", help="Print all information to screen.",default=False,action="store_true")
    parser.add_argument("-t", "--test", help="Run, but do not delete any attributes.",default=False,action="store_true")
    parser.add_argument("-s", "--silent", help="run in silent mode, output only to syslog",action="store_true",default=False)
    localArgs=parser.parse_args()

    return localArgs

def log_message (message, localArgs, logTypeJSON=False):
    """Writes log message to screen or syslog depending on where the script is run
 
    :param message: string to be written to log
    :param localArgs: argument parsers from system
    :param logTypeJSON: boolean default False - Set to true if log type is in json format
    """   
    

    if localArgs.inter:
        if (localArgs.verbose == True and logTypeJSON == True):
            print(json.dumps(message, indent=2))
        elif (logTypeJSON == False):
            print (message)
    else:
        syslog.syslog(json.dumps(message, indent=2))

def setup_logging(localArgs):
    """setup logging to screen or syslog and setup syslog
 
    :param localArgs: argument parsers from system
    """  
    # see if we are interactive shell
    if sys.stdin.isatty():
        if localArgs.silent:
            localArgs.inter = False
        else:
            localArgs.inter = True
    else:
        localArgs.inter = False
    
    # configure SYSLOG
    if ( localArgs.inter == False ):
        syslog.openlog(logoption=syslog.LOG_PID)
      
    log_message ( ("version {} started".format(VERSION)), localArgs)
    if ( localArgs.test ):
        log_message ( "Test mode enabled, no changes will be made to system", localArgs)

    return localArgs

def login_py_misp(url, key, localArgs):
    """login to MISP server - parameters usally defined by keys.py file
 
    :param url: url of MISP server
    :param key: authorization key 
    :param localArgs: argument parsers from system
    """

    # disable waringings about ssl certificates by default
    if (localArgs.verbose != True):
        warnings.simplefilter("ignore")
    return PyMISP(url, key, misp_verifycert, 'json')

def modify_expired_attributes( searchTag, timeFrame, localArgs ):
    """Finds expired attributes, removes searchTag, and tags the attribute as expired
 
    :param searchTag: tag in MISP to search through and find tagged attributes
    :param timeFrame: time in seconds that defines when an attribute is expired
    :param localArgs: Arguments passed on from command line parser
    """
    attributeCount=0
    attributeExpiredCount=0
    #Search for tag
    searchResults = misp.search(controller='attributes', tags=searchTag)
    #if the search results are empty break out of the function, and print record
    checkEmpty = searchResults['response']
    if (not(checkEmpty)):
        log_message (( 'No attribues found with Tag: ' + searchTag ), localArgs)
        return
    # Loop through results, find tagged attributes, and count them
    for foundAttribute in searchResults['response']['Attribute']:
        attributeCount+=1
        log_message( foundAttribute, localArgs, True)
        #Modify the attribute if it's older than alloted time
        if (TIMECURRENT > ( int(foundAttribute['timestamp']) + timeFrame) ):
            attributeExpiredCount+=1
            if (localArgs.test):
                log_message( ("Test Mode: Found Expired Attribute: " + foundAttribute['id'] + "  Tag: " + searchTag + " Attribute Time: " + foundAttribute['timestamp'] ), localArgs)
            else:
                #Find published status of found event
                foundEvent = misp.get_event(foundAttribute['event_id'])
                log_message( foundEvent, localArgs, True)
                #Add expired tag, remove any blocking tags, change IDS flag to false
                misp.tag(foundAttribute['uuid'], TAG_EXPIRED)
                for blockingTag in REMOVE_BLOCKING_TAGS:
                    misp.untag(foundAttribute['uuid'], blockingTag)
                misp.untag(foundAttribute['uuid'], searchTag)
                misp.change_toids(foundAttribute['uuid'], False)
                #if event was published then republish event, do not email alert
                if (foundEvent['Event']['published'] == True):
                    misp.publish( foundAttribute['event_id'], False)
                log_message( ("Modified attribute: " + foundAttribute['id'] + " Search Tag: " + searchTag + " Event: " + str(foundAttribute['event_id']) + " Event Published: " + str(foundEvent['Event']['published']) ), localArgs)
    #Print the nubmer of attribures found and expired
    if (localArgs.test):
        log_message ( ("Tag: " + searchTag + " Found Attributes: " + str(attributeCount) + " Expired Attributes: " + str(attributeExpiredCount)) , localArgs)
    else:
        log_message ( ("Tag: " + searchTag + " Found Attributes: " + str(attributeCount) + " Modified Attributes: " + str(attributeExpiredCount)) , localArgs)

        
##############
# Main
##############
if __name__ == '__main__':
    
    ##################
    #Get arguments, Setup logging
    ##################
    args = get_args()
    args = setup_logging(args)  

    ###################
    # Login in and start checking for expired tags
    ###################
    misp = login_py_misp(misp_url, misp_key, args)
    
    #Run for 30/60/90/180/365 day tags
    modify_expired_attributes(TAG30DAY, TIME30DAYS, args)
    modify_expired_attributes(TAG60DAY, TIME60DAYS, args)
    modify_expired_attributes(TAG90DAY, TIME90DAYS, args)
    modify_expired_attributes(TAG180DAY, TIME180DAYS, args)
    modify_expired_attributes(TAG365DAY, TIME365DAYS, args)